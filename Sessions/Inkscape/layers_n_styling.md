## Design problem

### Layers and composition

<p align = "center">
<img width="50%" src = "https://i.pinimg.com/originals/ce/68/d5/ce68d5d6a2bae87a11e0d8190df2e3ac.jpg">
</p>
<p align = "center">
Create an image identical to this.
</p>

<details>
  <summary>Video tutorial</summary>
</details>

<p align = "center">
<img width="50%" src = "https://1.bp.blogspot.com/-eh1MBgq5Ys0/XqrEO-6Fc1I/AAAAAAAAQrU/MOu21QvIC00iaXPv6Jku40nzQwOgFhUlgCLcBGAsYHQ/s1600/street%2Btext%2Beffect%2B2%2Bby%2Bfreeject.net.jpg">
</p>
<p align = "center">
Create an image identical to this.
</p>
<details>
  <summary>Video tutorial</summary>
</details>