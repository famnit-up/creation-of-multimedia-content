## Design problem

### Vectorization and pen tool

<p align = "center">
<img width="50%" src = "https://static.wikia.nocookie.net/kungfupanda/images/7/73/KFP3-promo-po4.jpg/revision/latest?cb=20150726165358">
</p>
<p align = "center">
Create an image identical to this.
</p>

<details>
  <summary>Exercise</summary>
  <a href="./hommework/panda.svg">Download the panda to finlize the lab work</a>
</details>
