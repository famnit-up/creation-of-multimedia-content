# Inkscape 

## A little intro...
>Inkscape is a free and open-source vector graphics editor used to create vector images, primarily in Scalable Vector Graphics (SVG) format.

We are aware that [online](https://inkscape.org/learn/tutorials/) there are already extensive and comprehensive tutorials about inkscape and many other editors. Nevertheless, we do not want you to master this editor as a profesional but to lear how and when to use them in a creative maner. 

In this first tutorial we will explore the interface and try to solve some challenges.

But before start,lets open Inkscape and explore the interface.

## Configuration and interface

The first time you Inkscape you will be prompt with a view similar to the one in Figure 1.
<p align = "center">
<img width="50%" src = "https://drive.google.com/uc?export=view&id=1J09Parwk4bVxTfNE6q-OKL9qu-8oSFNS">
</p>
<p align = "center">
Fig.1 - Setup screen
</p>

This view is where you can customize the interface, for example you can change betweeen dark and light mode. You can use whatever setup you feel more comfotable with, however, I encourage you to use the *Classic inkscape appereance* so you and the ones that have different version of the software can follow the tutorials easily. See Figure 2 to check my configuration.

<p align = "center">
<img width="50%" src = "https://drive.google.com/uc?export=view&id=1TsXtw2oApqkz_tXPq2Mn9Zmq44lFqw9v">
</p>
<p align = "center">
Fig.2 - My configuration
</p>

I'll skip one view (*Supported by You*) and go  directly to the tab *Time to draw*. As you can see in this view (Figure 3), we can open (if we have) *svg* files (this is the format that Inkscape uses to save files). For now, this screen it might be empty but it will get populated as we move on in this lab sessions.

<p align = "center">
<img width="50%" src = "https://drive.google.com/uc?export=view&id=1ILCmOoMkxuyRW16inRTGv5LI7pJ4D1G6">
</p>
<p align = "center">
Fig.3 - HUB view
</p>

In the left side of the *Time to draw* view you can observe a list or "targets", each of them represent a device or medium where the document that you create in Inkscape are intended to be displayed or reproduced. Try to click on them to see the available presets. 

> Challenge 1. Create a *new document* for printing of size *A4*

<details>
  <summary>Solution challenge </summary>
  | ![space-1.jpg](https://drive.google.com/uc?export=view&id=1opRcmVVb6GX2owgmmcfncyHGp2LcH5BV) |
|:--:|
| <i>Step 1: click on *Print*. Step 2: Select *A4 preset*. Step 3: Press the button *New Document*</i>|
</details>

Well, probably not much of challenge but at least we know how to create a new document.

With our new document open, we will presented with a new view (see Figure 4), that we will call the *main view*. The *main view* is the interfsace we will interact the most so it will be nice to remember some sections and bars.

<p align = "center">
<img width="50%" src = "https://drive.google.com/uc?export=view&id=1Q3EI7HgXR6Lij5GI6WJ737zoxLehpiHj">
</p>
<p align = "center">
Fig.4 - Main view. Red zone, system controls. Yellow zone, artboard tools. Green zone, edition tools. Blue zone, artboard.
</p>

Overall it is important to get familiar at least with the [edition tools](https://commons.wikimedia.org/wiki/File:Inkscape_toolbar.png) and the artboard.

But as we said before all this documentation is already there on the internet. Now let learn more about the Inkscape but by solving specific design problems.

## Design problem

### Shapes
<p align = "center">
<img width="50%" src = "https://drive.google.com/uc?export=view&id=1AQgDWckhvTBaF63stL2L7lHIkn6vACDG">
</p>
<p align = "center">
Create an image identical to this.
</p>

<details>
  <summary>Video tutorial</summary>
</details>

### Text

<p align = "center">
<img width="50%" src = "https://i.insider.com/5d3762ee36e03c314d1f5255?width=1200&format=jpeg">
</p>
<p align = "center">
Create an image identical to this.
</p>
<details>
  <summary>Video tutorial</summary>
</details>
